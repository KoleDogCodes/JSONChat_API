<?php

namespace App\Listeners;

use App\Events\SendMessageEvent;
use App\Notify;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMessageListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendMessageEvent  $event
     * @return void
     */
    public function handle(SendMessageEvent $event)
    {
        if ($this->notificationExists($event->group_id)){
            Notify::where('group_id', '=', $event->group_id)->update([
                'title' => 'New Message'
            ]);
        }
        else {
            Notify::create(
                [
                    "group_id" => $event->group_id,
                    "title" => "New Message"
                ]
            );
        }
    }

    public function notificationExists($group_id){
        $result = Notify::where('group_id', '=', $group_id)->get();

        if ($result != null && $result != []){
            return true;
        }
        else {
            return false;
        }
    }
}
