<?php

namespace App\Http\Controllers;

use App\User;
use App\Users;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function checkLogin(){
//        if (Session::has('display_name')){
//            return JSONUtili::createJSONObject(["auth", "username"], ['true', Session::get('display_name')]);
//        }
//        else {
//            return JSONUtili::createJSONObject(["auth"], ['false']);
//        }
        return Auth::check();
    }

//    public function login(Request $request){
//        $username = $request->get('username');
//        $password = $this->hash($request->get('password'));
//
//        if ($this->validateLogin($username, $password)){
//            Session::put('display_name', $username);
//            return JSONUtili::createJSONObject(["auth", "username"], ["true", Session::get('display_name')]);
//        }
//        else {
//            return JSONUtili::createJSONObject(["auth"], ["Fail"]);
//        }
//    }

//    public function register(Request $request){
//        $username = $request->get("username");
//        $email = $request->get("email");
//        $password = $request->get("password");
//        $verifyPassword = $request->get("verify_password");
//
//        if ($password != $verifyPassword){
//            return JSONUtili::createJSONObject(['mode', 'result', 'error'], ['' . Session::has('display_name'), 'false', 'Passwords do not match']);
//        }
//
//        if (Users::userExists($username)) {
//            return JSONUtili::createJSONObject(['mode', 'result', 'error'], ['' . Session::has('display_name'), 'false', 'Username already exists']);
//        }
//        else if (Users::emailExists($username)) {
//            return JSONUtili::createJSONObject(['mode', 'result', 'error'], ['' . Session::has('display_name'), 'false', 'Email already exists']);
//        }
//        else {
//            $this->createAccount($username, $email, $password);
//            return JSONUtili::createJSONObject(['mode', 'result'], ['' . Session::has('display_name'), 'true']);
//        }
//    }

//    public function validateLogin($username, $password){
//        $result = Users::where('name', 'LIKE', $username)->where('password', '=', $password)->first();
//        $result_fallback = Users::where('email', 'LIKE', $username)->where('password', '=', $password)->first();
//
//        if ($result != null && $result != []){
//            return true;
//        }
//        else if ($result_fallback != null && $result_fallback != []){
//            return true;
//        }
//        else {
//            return false;
//        }
//    }
//
//    public function createAccount($username, $email, $password){
//        Users::create(
//            [
//                'name' => $username,
//                'email' => $email,
//                'password' => $this->hash($password),
//                'status' => 'Offline',
//                'activated' => false
//            ]);
//    }
//
//    public function logout(){
//        Session::remove('display_name');
//        return JSONUtili::createJSONObject(["auth", "result"], ['false', "You have successfully logged out."]);
//    }
//
//    public function hash($string){
//        return hash_hmac("sha512", $string . getenv('APP_KEY'), getenv('APP_KEY'));
//    }
}
