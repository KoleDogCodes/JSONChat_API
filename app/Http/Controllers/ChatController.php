<?php

namespace App\Http\Controllers;

use App\Events\SendMessageEvent;
use App\Messages;
use App\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ChatController extends Controller
{
    public function getMessages($id){
//        if (!Session::has('display_name')){
//            return JSONUtili::createJSONObject(["auth"], ['false']);
//        }
//
//        $user_id = Users::getUserId(Session::get('display_name'));
//
//        if ($user_id <= -1){
//            return JSONUtili::createJSONObject(["error"], ["No user id found!"]);
//        }
        $user_id = \Auth::user()->id;

        $result = Messages::where('group_id', '=', $id)->where('user_id', '=', $user_id)->get();

        if ($result == null){
            $result = array();
        }
        else {
            $result = $result->toArray();
        }

        return json_encode($result);
    }

    public function getGroups(){
//        if (!Session::has('display_name')){
//            return JSONUtili::createJSONObject(["auth"], ['false']);
//        }
//
//        $user_id = Users::getUserId(Session::get('display_name'));
//
//        if ($user_id <= -1){
//            return JSONUtili::createJSONObject(["error"], ["No user id found!"]);
//        }
        $user_id = \Auth::user()->id;

        $result = Messages::where('user_id', '=', $user_id)->get();

        if ($result == null){
            $result = array();
        }
        else {
            $result = $result->toArray();
        }

        return json_encode($result);
    }

    public function addMessage(Request $request){
//        if (!Session::has('display_name')){
//            return JSONUtili::createJSONObject(["auth"], ['false']);
//        }
//
//        $user_id = Users::getUserId(Session::get('display_name'));
//
//        if ($user_id <= -1){
//            return JSONUtili::createJSONObject(["error"], ["No user id found!"]);
//        }
        $user_id = \Auth::user()->id;

        $message = $request->get('message');
        $group_id = $request->get('group_id');

        Messages::create([
                'user_id' => $user_id,
                'group_id' => $group_id,
                'read' => 'false',
                'message' => $message
            ]);

        event(new SendMessageEvent($group_id, $message));

        return JSONUtili::createJSONObject(["auth", "sent"], ['true', 'true']);
    }

}
