<?php

namespace App\Providers;

use App\User;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Quezler\Gravely\Enums\Char;

class TokenServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        User::created(function (User $user) {

            $signer = new Sha256();
            $builder = new Builder();

            $builder
                ->setIssuer(url('/'))
                ->setAudience(url('/'))
                ->setIssuedAt(time())
                ->setNotBefore(0)
                ->setExpiration(2147483647)
                ->setSubject(implode('.', ['user', $user->id]))
                ->sign($signer, config('app.key'))
            ;

            $user->api_token = $builder->getToken()->__toString();
            $user->save();
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
